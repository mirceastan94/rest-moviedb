package com.movies.restservice;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.movies.restservice.entity.Movie;
import com.movies.restservice.entity.User;
import com.movies.restservice.repository.MovieRepository;
import com.movies.restservice.repository.UserRepository;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	protected CommandLineRunner init(final UserRepository userRepository) {

		return args -> {
			User defaultUser = new User();
			defaultUser.setName("Admin");
			defaultUser.setEmail("mobu@gmail.com");
			defaultUser.setUsername("root");
			defaultUser.setPassword("root");
			userRepository.save(defaultUser);
		};
	}
}