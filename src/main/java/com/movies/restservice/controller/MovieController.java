package com.movies.restservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movies.restservice.entity.Movie;
import com.movies.restservice.repository.MovieRepository;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(path = "/movies")
public class MovieController {

	@Autowired
	private MovieRepository repository;

	@GetMapping
	public Iterable<Movie> findAll() {
		return repository.findAll();
	}

	@GetMapping(path = "/{title}")
	public Movie find(@PathVariable("title") String title) {
		return repository.findOne(title);
	}

	@PostMapping(consumes = "application/json")
	public Movie create(@RequestBody Movie movie) {
		return repository.save(movie);
	}

	@DeleteMapping(path = "/{title}")
	public void delete(@PathVariable("title") String title) {
		repository.delete(title);
	}

	@PostMapping(path = "/{title}")
	public Movie update(@PathVariable("title") String title, @RequestBody Movie movie) throws BadHttpRequest {
		if (repository.exists(title)) {
			movie.setTitle(title);
			return repository.save(movie);
		} else {
			throw new BadHttpRequest();
		}
	}
}