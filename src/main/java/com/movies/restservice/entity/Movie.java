package com.movies.restservice.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Movie {

	@Id
	private String title;
	private String movieDescription;
	private String releaseDate;
	private Double voteAverage;

	public String getTitle() {
		return title;
	}

	public void setTitle(String username) {
		this.title = username;
	}

	public String getMovieDescription() {
		return movieDescription;
	}

	public void setMovieDescription(String password) {
		this.movieDescription = password;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Double getVoteAverage() {
		return voteAverage;
	}

	public void setVoteAverage(Double voteAverage) {
		this.voteAverage = voteAverage;
	}

	@Override
	public String toString() {
		return "Movie [title=" + title + ", movieDescription=" + movieDescription + ", releaseDate=" + releaseDate
				+ ", voteAverage=" + voteAverage + "]";
	}

}