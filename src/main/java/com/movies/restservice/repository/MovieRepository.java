package com.movies.restservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.movies.restservice.entity.Movie;

@RestResource(exported = false)
public interface MovieRepository extends JpaRepository<Movie, String> {

}